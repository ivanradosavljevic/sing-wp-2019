    var app = angular.module('myApp', []);
    app.controller('MyCtrl', function($scope, $window, $http) {
      var vm = this;


      vm.zaposleni = [{"ime":"Djordje","prezime":"Obradovic","plata":"100"},{"ime":"Marko","prezime":"Markovic","plata":"200"}];

      vm.index = 0;
      vm.ime = "";
      vm.prezime = "";
      vm.plata = 10;

      vm.brojac = 0
      vm.obrada = function(){
        vm.brojac++;
        if(vm.brojac == 2){
          console.log("obrada"+vm.brojac);
        }
      }


      vm.asinhroniPoziv = function(){

        var elementi = [];

        $http({
          method: 'GET',
          url: '/test_1'
        }).then(function(resp){
          console.log('OK test1');
          vm.zaposleni = resp.data;
          elementi.push('1');
          vm.obrada();
        }, function(resp){
          console.log('Error');
        })

        $http({
          method: 'GET',
          url: '/test_2'
        }).then(function(resp){
          console.log('OK test2');
          vm.zaposleni = resp.data;
          vm.obrada();
        }, function(resp){
          console.log('Error');
        })

        //console.log('obrada');
      


      }



      vm.init = function(){
        $http({
          method: 'GET',
          url: '/zaposleni'
        }).then(function(resp){
          console.log('OK');
          vm.zaposleni = resp.data;
        }, function(resp){
          console.log('Error');
        })
      }

      vm.sacuvaj = function(){
        var zaposleni = {
            index: vm.index,
            ime: vm.ime,
            prezime: vm.prezime,
            plata: vm.plata
          }
        $http({
          method: 'PUT',
          data: zaposleni,
          url: '/zaposleni_update'
        }).then(function(resp){
          console.log('OK');
          vm.zaposleni = resp.data;
        }, function(resp){
          console.log('Error');
        })
      }

      vm.selektuj = function(index){
          vm.index = index;
          vm.ime = vm.zaposleni[index].ime;
          vm.prezime = vm.zaposleni[index].prezime;
          vm.plata = vm.zaposleni[index].plata;
      }

      vm.delete = function(index){
        var data = {
            index: index
          }
        $http({
          method: 'PUT',
          data: data,
          url: '/zaposleni_delete'
        }).then(function(resp){
          console.log('OK');
          vm.zaposleni = resp.data;
        }, function(resp){
          console.log('Error');
        })
      }

      vm.dodaj = function(){
        var zaposleni = {
            ime: vm.ime,
            prezime: vm.prezime,
            plata: vm.plata
          }
        $http({
          method: 'PUT',
          data: zaposleni,
          url: '/zaposleni'
        }).then(function(resp){
          console.log('OK');
          vm.zaposleni = resp.data;
        }, function(resp){
          console.log('Error');
        })


        // vm.zaposleni.push(
        //   {
        //     ime: vm.ime,
        //     prezime: vm.prezime,
        //     plata: vm.plata
        //   }
        // );
      }


      vm.init();
    });
