import flask
from flask import Flask
from flask import request
import json
import time

app = Flask(__name__, static_url_path="")

korisnici = []

@app.route("/")
@app.route("/index")
@app.route("/index.html")
def index_page():
    return flask.send_file("index.html")

@app.route("/zaposleni", methods=["GET"])
def zaposleni():
    with open("zaposleni.json", "r") as file:
        lista = json.load(file)
    # lista = [
    #     {"ime":"Djordje","prezime":"Obradovic","plata":"100"},
    #     {"ime":"Marko","prezime":"Obradovic","plata":"300"},
    #     {"ime":"Marko","prezime":"Markovic","plata":"200"}
    # ];
    return flask.jsonify(lista)


@app.route("/zaposleni", methods=["PUT"])
def zaposleni_add():
    data = request.json
    print(data)
    with open("zaposleni.json", "r") as file:
        lista = json.load(file)
    lista.append(data)
    with open("zaposleni.json", "w") as file:
        json.dump(lista, file, indent=4)
    return flask.jsonify(lista)

@app.route("/zaposleni_delete", methods=["PUT"])
def zaposleni_delete():
    data = request.json
    print(data)
    index = int(data['index'])
    print(index)
    with open("zaposleni.json", "r") as file:
        lista = json.load(file)
    del lista[index]
    with open("zaposleni.json", "w") as file:
        json.dump(lista, file, indent=4)
    return flask.jsonify(lista)

@app.route("/zaposleni_update", methods=["PUT"])
def zaposleni_update():
    data = request.json
    print(data)
    index = int(data['index'])
    print(index)
    with open("zaposleni.json", "r") as file:
        lista = json.load(file)
    lista[index] = data
    with open("zaposleni.json", "w") as file:
        json.dump(lista, file, indent=4)
    return flask.jsonify(lista)

@app.route("/test_1", methods=["GET"])
def test_1():
    print('zahtev stigao u test 1')
    time.sleep(1)
    print('zahtev test 1 obradjen')
    return flask.jsonify(['1'])

@app.route("/test_2", methods=["GET"])
def test_2():
    print('zahtev stigao u test 2')
    time.sleep(3)
    print('zahtev test 2 obradjen')
    return flask.jsonify(['2'])



@app.route("/registracija", methods=["POST"])
def registracija():
    korisnici.append({
            "ime": flask.request.form["ime"],
            "prezime": flask.request.form["prezime"],
            "email": flask.request.form["email"],
            "kime": flask.request.form["kime"],
            "lozinka": flask.request.form["lozinka"],
            "drodjenja": flask.request.form["drodjenja"]
        })
    return flask.render_template("korisnik.tpl.html")

# @app.route("/korisnici")
# def prikaz_tabele_korisnika():
#     return flask.render_template("korisnici.tpl.html", korisnici=korisnici)

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)

