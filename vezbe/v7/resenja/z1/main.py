import flask
import datetime
from flask import Flask

# Kreira se instanca flask aplikacije.
# Argument static_url_path odredjuje prefiks URL-a na koji ce se mapirati
# sadrzaj direktorijuma static. Podrazumevano je static. U ovom slucaju
# podeseno je da se mapira na korenski URL. To znaci da se zarad pristupanja
# statickim datotekama na serveru ne mora kucati URL localhost:5000/static/ime_datoteke
# vec da je dovoljno uneti localhost:5000/ime_datoteke. Sav staticki sadrzaj
# i dalje mora biti u direktorijumu static.
app = Flask(__name__, static_url_path="")
 
 # Globalna promenljiva koja predstavlja kolekciju proizvoda. !Veoma losa praksa. Upotrebljivo samo kao primer!
proizvodi = [
    {
        "naziv": "Proizvod 1",
        "opis": "Opis proizvoda 1",
        "kolicina": 2,
        "cena": 10.2
    },
    {
        "naziv": "Proizvod 2",
        "opis": "Opis proizvoda 2",
        "kolicina": 0,
        "cena": 4.3
    },
    {
        "naziv": "Proizvod 3",
        "opis": "Opis proizvoda 3",
        "kolicina": 5,
        "cena": 6.1
    }
]

# Definisan je obradjivac zahteva upucenih na korenski URL.
# Moguce je navesti vise ruta za jedan obradjivac zahteva.
@app.route("/")
@app.route("/index")
@app.route("/index.html")
# Sve rute se mapiraju na funkciju odmah ispod njih.
# Vezivanje ruta za funkciju realizovano je upotrebom dekoratora.
def index_page():
    # Fukncija render template kao argumente prima putanju do sablona
    # na osnovu kojeg se generise sadrzaj i promenljive koje ce biti
    # upotrebljene za generisanje sadrzaja. Promenljive se prosledjuju
    # kao keyword argumenti, odnoso parovi kljuc vrednost u formatu
    # kljuc=vrednost. Pored prosledjenih promenljivih pri generisanju
    # sadrzaja dostupne se u i ugradjene promenljive poput promenljive
    # request. Putanja do sablona je relativna u odnosu na putanju
    # zadatu argumentom template_folder pri konstruisanju instance klase Flask.
    # Podrazumevana vrednost template_folder argumenta je templates
    # sto znaci da svi sabloni treba da se nalaze u direktorijumu
    # templates.
    # Obradjivac zahteva kao odgovor na zahtev sada salje
    # sadrzaj generisan na osnovu sablona index.tpl.html.
    return flask.render_template("index.tpl.html", proizvodi=proizvodi)

# Ukoliko je zahtev upucen na URL /proizvodi i pri tome
# je tip zahteva GET funkcija prikaz_svih_proizvoda ce vrsiti obradu
# zahteva.
@app.route("/proizvodi", methods=["GET"])
def prikaz_svih_proizvoda():
    # Vraca se sadrzaj koji je izgenerisan na osnovu sablona za prikaz
    # svih proizvoda.
    return flask.render_template("proizvodi.tpl.html", proizvodi=proizvodi)

# Ukoliko je zahtev upucen na URL /proizvodi/INDEKS_PROIZVODA i pri tome
# je tip zahteva GET funkcija priakz_proizvoda ce vrsiti obradu
# zahteva. Ruta je parametrizovana tako da moze primiti proizvoljan
# indeks proizvoda.
@app.route("/proizvodi/<int:indeks>", methods=["GET"])
def priakz_proizvoda(indeks): # Indeks iz rute se prosledjuje pri pozivu obradjivaca i dalje dostupan u telu funkcije.
    # Vraca se sadrzaj koji je izgenerisan na osnovu sablona za prikaz jednog proizvoda.
    return flask.render_template("proizvod.tpl.html", proizvod=proizvodi[indeks])

# Ukoliko je zahtev upucen na URL /proizvodi i pri tome
# je tip zahteva POST funkcija dodaj_proizvod ce vrsiti obradu
# zahteva.
@app.route("/proizvodi", methods=["POST"])
def dodaj_proizvod():
    # Prilikom dodavanja proizvoda kopiraju se vrednosti
    # iz request.form objekta. Ovo je neophodno jer
    # form atribut sadrzi objekat tipa ImmutableMultiDict
    # sto predstavlja nepromenljivi recnik koji na jednom
    # kljucu sadrzi vise vrednosti. Na jednom kljucu je
    # dozvoljeno vise vrednosti jer se u formi moze pojaviti
    # vise input polja sa istom vrednoscu atributa name.
    # Kako ce u kasnijim zadacima biti neophodno menjati
    # vrednosti koje su prosledjene u formi neophodno je
    # ImmutableMultiDict objekat pretvoriti u obican
    # promenljivi recnik. Moguce je na kraci nacin pretvoriti
    # ImmutableMultiDict u obican recnik ali radi jednostavnosti
    # koda bice koriscen ovaj nacin.
    proizvodi.append({
            "naziv": flask.request.form["naziv"],
            "opis": flask.request.form["opis"],
            "kolicina": int(flask.request.form["kolicina"]), # Podaci iz forme su stringovi,
            "cena": float(flask.request.form["cena"])        # neophodno ih je pretvoriti u ispravne
                                                             # tipove radi dalje upotrebe
        })
    # Fukncija redirect kao argument prima URL do resursa na koji
    # je neophodno preusmeriti zahtev. U ovom slucaju preusmeravanje
    # se vrsi na stranicu proizvoda kako bi se nakon dodavanja proizvoda
    # prikazala tabela svih proizvoda
    return flask.redirect("/proizvodi")

# Ukoliko je zahtev upucen na URL /proizvodi/izmena/INDEKS_PROIZVODA i pri tome
# je tip zahteva GET funkcija izmeni_proizvod_forma ce vrsiti obradu
# zahteva. Ruta je parametrizovana tako da moze primiti proizvoljan
# indeks proizvoda.
@app.route("/proizvodi/izmena/<int:indeks>", methods=["GET"])
def izmeni_proizvod_forma(indeks):
    # Vraca se sadrzaj koji je izgenerisan na osnovu sablona za prikaz forme za izmenu proizvoda.
    return flask.render_template("izmena.tpl.html", indeks=indeks, proizvod=proizvodi[indeks])

# Ukoliko je zahtev upucen na URL /proizvodi/izmeni/INDEKS_PROIZVODA i pri tome
# je tip zahteva POST funkcija izmeni_proizvod ce vrsiti obradu
# zahteva. Ruta je parametrizovana tako da moze primiti proizvoljan
# indeks proizvoda.
@app.route("/proizvodi/izmeni/<int:indeks>", methods=["POST"])
def izmeni_proizvod(indeks):
    # Vrsi se izmena recnika na zadatom indeksu novim recnikom
    # koji predstavlja kopiju sadrzaja forme za izmenu.
    proizvodi[indeks] = {
        "naziv": flask.request.form["naziv"],
        "opis": flask.request.form["opis"],
        "kolicina": int(flask.request.form["kolicina"]), # Podaci iz forme su stringovi,
        "cena": float(flask.request.form["cena"])        # neophodno ih je pretvoriti u ispravne
                                                         # tipove radi dalje upotrebe
    }
    # Fukncija redirect kao argument prima URL do resursa na koji
    # je neophodno preusmeriti zahtev. U ovom slucaju preusmeravanje
    # se vrsi na stranicu proizvoda kako bi se nakon dodavanja proizvoda
    # prikazala tabela svih proizvoda
    return flask.redirect("/proizvodi")

# Ukoliko je zahtev upucen na URL /proizvodi/ukloni/INDEKS_PROIZVODA i pri tome
# je tip zahteva GET funkcija ukloni_proizvod ce vrsiti obradu
# zahteva. Ruta je parametrizovana tako da moze primiti proizvoljan
# indeks proizvoda.
@app.route("/proizvodi/ukloni/<int:indeks>", methods=["GET"])
def ukloni_proizvod(indeks):
    del proizvodi[indeks] # Proizvod na zadatom indeksu se uklanja iz liste.
    return flask.redirect("/proizvodi")

# Kako se sada aplikacija pokrece u odvojenom
# kontejneru a ne direktno kao python skritpa
# neophodno je dodati proveru da li je modul
# pokrenut direktno preko interpretera. Ukoliko
# je modul pokrenut direktno vrednost promenljive
# __name__ ce biti string __main__ pa ce se
# aplikacija pokrenuti pozivom metode run nad
# instancom klase Flask. Ukoliko je modul
# pokrenut iz kontejnera ovaj uslov nece biti
# zadovoljen pa se ni poziv metode run nece desiti
# nego ce kontejner izvrsiti pokretanje Flask aplikacije
# onako kako je to u izabranom kontejneru predvidjeno.
if __name__ == "__main__":
    # Aplikacija se pokrece tako da joj je moguce pristupa
    # od "spolja", odnosno sa udaljenih racunara. Ovo je postignuto
    # postavljanjem vrednosti prvog argumenta na ip adresu 0.0.0.0.
    # Potom je, narednim argumentom, navedeno da aplikacija slusa na port 5000.
    # Postavljanje vrednosti imenovanog argumenta threaded na True dovodi do toga
    # da ce se svaki zahtev obradjivati u odvojenom thread-u.
    # To znaci da prilikom obrade jednog zahteva nece doci do blokiranja
    # izvrsavanja cele aplikacije ili obrade drugih zahteva.
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.