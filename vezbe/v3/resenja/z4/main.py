import flask
from flask import Flask

# Kreira se instanca flask aplikacije.
# Argument static_url_path odredjuje prefiks URL-a na koji ce se mapirati
# sadrzaj direktorijuma static. Podrazumevano je static. U ovom slucaju
# podeseno je da se mapira na korenski URL. To znaci da se zarad pristupanja
# statickim datotekama na serveru ne mora kucati URL localhost:5000/static/ime_datoteke
# vec da je dovoljno uneti localhost:5000/ime_datoteke. Sav staticki sadrzaj
# i dalje mora biti u direktorijumu static.
app = Flask(__name__, static_url_path="")

korisnici = [] # Globalna promenljiva koja predstavlja kolekciju korisnika. !Veoma losa praksa. Upotrebljivo samo kao primer!

# Definisan je obradjivac zahteva upucenih na korenski URL.
# Moguce je navesti vise ruta za jedan obradjivac zahteva.
@app.route("/")
@app.route("/index")
@app.route("/index.html")
# Sve rute se mapiraju na funkciju odmah ispod njih.
# Vezivanje ruta za funkciju realizovano je upotrebom dekoratora.
def index_page():
    # Obradjivac zahteva kao odgovor na zahtev sada salje sadrzaj datoteka
    # forma_post.html. Funkcija send_file prima putanju do datoteke ciji sadrzaj je potrebno
    # poslati kao odgovor. Putanja je relativna u odnosu na putanju sa koje se
    # izvrsava skripta.
    return flask.send_file("index.html")

# Ukoliko je zahtev upucen na URL /registracija i pri tome
# je tip zahteva POST funkcija registracija ce vrsiti obradu
# zahteva.
@app.route("/registracija", methods=["POST"])
def registracija():
    # Prilikom dodavanja korisnika kopiraju se vrednosti
    # iz request.form objekta. Ovo je neophodno jer
    # form atribut sadrzi objekat tipa ImmutableMultiDict
    # sto predstavlja nepromenljivi recnik koji na jednom
    # kljucu sadrzi vise vrednosti. Na jednom kljucu je
    # dozvoljeno vise vrednosti jer se u formi moze pojaviti
    # vise input polja sa istom vrednoscu atributa name.
    # Kako ce u kasnijim zadacima biti neophodno menjati
    # vrednosti koje su prosledjene u formi neophodno je
    # ImmutableMultiDict objekat pretvoriti u obican
    # promenljivi recnik. Moguce je na kraci nacin pretvoriti
    # ImmutableMultiDict u obican recnik ali radi jednostavnosti
    # koda bice koriscen ovaj nacin.
    korisnici.append({
            "ime": flask.request.form["ime"],
            "prezime": flask.request.form["prezime"],
            "email": flask.request.form["email"],
            "kime": flask.request.form["kime"],
            "lozinka": flask.request.form["lozinka"],
            "drodjenja": flask.request.form["drodjenja"]
        })
    # Fukncija render template kao argumente prima putanju do sablona
    # na osnovu kojeg se generise sadrzaj i promenljive koje ce biti
    # upotrebljene za generisanje sadrzaja. Promenljive se prosledjuju
    # kao keyword argumenti, odnoso parovi kljuc vrednost u formatu
    # kljuc=vrednost. Pored prosledjenih promenljivih pri generisanju
    # sadrzaja dostupne se u i ugradjene promenljive poput promenljive
    # request. Putanja do sablona je relativna u odnosu na putanju
    # zadatu argumentom template_folder pri konstruisanju instance klase Flask.
    # Podrazumevana vrednost template_folder argumenta je templates
    # sto znaci da svi sabloni treba da se nalaze u direktorijumu
    # templates.
    return flask.render_template("korisnik.tpl.html")

@app.route("/korisnici")
def prikaz_tabele_korisnika():
    # U kontekst sablona se prosledjuje promenljiva korisnicic i pridruzuje
    # joj se naziv korisnici. U sablonu ce se ovoj promenljivoj pristupati
    # preko pridruzenog naziva.
    return flask.render_template("korisnici.tpl.html", korisnici=korisnici)

# Kako se sada aplikacija pokrece u odvojenom
# kontejneru a ne direktno kao python skritpa
# neophodno je dodati proveru da li je modul
# pokrenut direktno preko interpretera. Ukoliko
# je modul pokrenut direktno vrednost promenljive
# __name__ ce biti string __main__ pa ce se
# aplikacija pokrenuti pozivom metode run nad
# instancom klase Flask. Ukoliko je modul
# pokrenut iz kontejnera ovaj uslov nece biti
# zadovoljen pa se ni poziv metode run nece desiti
# nego ce kontejner izvrsiti pokretanje Flask aplikacije
# onako kako je to u izabranom kontejneru predvidjeno.
if __name__ == "__main__":
    # Aplikacija se pokrece tako da joj je moguce pristupa
    # od "spolja", odnosno sa udaljenih racunara. Ovo je postignuto
    # postavljanjem vrednosti prvog argumenta na ip adresu 0.0.0.0.
    # Potom je, narednim argumentom, navedeno da aplikacija slusa na port 5000.
    # Postavljanje vrednosti imenovanog argumenta threaded na True dovodi do toga
    # da ce se svaki zahtev obradjivati u odvojenom thread-u.
    # To znaci da prilikom obrade jednog zahteva nece doci do blokiranja
    # izvrsavanja cele aplikacije ili obrade drugih zahteva.
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.