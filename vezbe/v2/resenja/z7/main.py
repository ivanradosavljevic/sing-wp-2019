import flask
from flask import Flask

# Kreira se instanca flask aplikacije.
# Argument static_url_path odredjuje prefiks URL-a na koji ce se mapirati
# sadrzaj direktorijuma static. Podrazumevano je static. U ovom slucaju
# podeseno je da se mapira na korenski URL. To znaci da se zarad pristupanja
# statickim datotekama na serveru ne mora kucati URL localhost:5000/static/ime_datoteke
# vec da je dovoljno uneti localhost:5000/ime_datoteke. Sav staticki sadrzaj
# i dalje mora biti u direktorijumu static.
app = Flask(__name__, static_url_path="")

# Definisan je obradjivac zahteva upucenih na korenski URL.
@app.route("/")
def index_page():
    # Rezultat obrade zahteva je string koji predstavlja HTML stranicu.
    # Ne mogu se koristiti obicni navodnici, jer se string sastoji iz vise redova.
    # Mana ovakvog pristupa je nepregledan kod.
    return '''<!DOCTYPE html>
<html>
<head>
    <title>Registracija</title>
    <script src="main.js" defer></script>
</head>
<body>
    <!--Atribut akcija odredjuje gde ce biti upucen zahtev iz forme.-->
    <!--Atribut method odredjuje koji tip zahteva ce biti upucen, u ovom slucaju bice upucen GET zahtev.-->
    <form action="/registracija" method="GET">
        <div>
            <label for="ime">Ime:</label>
        </div>
        <div>
            <!--Atribut name ce biti upotrebljen za izdvajanje vrednosti polja na strani servera.-->
            <input type="text" id="ime" name="ime">
        </div>
        <div>
            <label for="prezime">Prezime:</label>
        </div>
        <div>
            <input type="text" id="prezime" name="prezime">
        </div>
        <div>
            <label for="email">Email:</label>
        </div>
        <div>
            <input type="email" id="email" name="email">
        </div>
        <div>
            <label for="kime">Korisničko ime:</label>
        </div>
        <div>
            <input type="text" id="kime" name="kime">
        </div>
        <div>
            <label for="lozinka">Lozinka:</label>
        </div>
        <div>
            <input type="password" id="lozinka" name="lozinka">
        </div>
        <div>
            <label for="plozinka">Ponovljena lozinka:</label>
        </div>
        <div>
            <input type="password" id="plozinka" name="plozinka">
        </div>
        <div>
            <label for="drodjenja">Datum rođenja:</label>
        </div>
        <div>
            <input type="date" id="drodjenja" name="drodjenja">
        </div>
        <div>
            <input type="submit" value="Registruj se">
        </div>
    </form>
</body>
</html>'''

# Moguce je navesti vise ruta za jedan obradjivac zahteva.
@app.route("/index")
@app.route("/index.html")
# Sve rute se mapiraju na funkciju odmah ispod njih.
# Vezivanje ruta za funkciju realizovano je upotrebom dekoratora.
def index_page_file():
    # Obradjivac zahteva kao odgovor na zahtev sada salje sadrzaj datoteka
    # index.html. Funkcija send_file prima putanju do datoteke ciji sadrzaj je potrebno
    # poslati kao odgovor. Putanja je relativna u odnosu na putanju sa koje se
    # izvrsava skripta.
    return flask.send_file("index.html")

# Aplikacija se pokrece tako da joj je moguce pristupa
# od "spolja", odnosno sa udaljenih racunara. Ovo je postignuto
# postavljanjem vrednosti prvog argumenta na ip adresu 0.0.0.0.
# Potom je, narednim argumentom, navedeno da aplikacija slusa na port 5000.
# Postavljanje vrednosti imenovanog argumenta threaded na True dovodi do toga
# da ce se svaki zahtev obradjivati u odvojenom thread-u.
# To znaci da prilikom obrade jednog zahteva nece doci do blokiranja
# izvrsavanja cele aplikacije ili obrade drugih zahteva.
app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.