import flask
from flask import Flask

# Kreira se instanca flask aplikacije.
# Argument static_url_path odredjuje prefiks URL-a na koji ce se mapirati
# sadrzaj direktorijuma static. Podrazumevano je static. U ovom slucaju
# podeseno je da se mapira na korenski URL. To znaci da se zarad pristupanja
# statickim datotekama na serveru ne mora kucati URL localhost:5000/static/ime_datoteke
# vec da je dovoljno uneti localhost:5000/ime_datoteke. Sav staticki sadrzaj
# i dalje mora biti u direktorijumu static.
app = Flask(__name__, static_url_path="")

# Definisan je obradjivac zahteva upucenih na korenski URL.
@app.route("/")
def index_page():
    # Obradjivac zahteva kao odgovor na zahtev sada salje sadrzaj datoteka
    # index.html. Funkcija send_file prima putanju do datoteke ciji sadrzaj je potrebno
    # poslati kao odgovor. Putanja je relativna u odnosu na putanju sa koje se
    # izvrsava skripta.
    return flask.send_file("index.html")

# Moguce je navesti vise ruta za jedan obradjivac zahteva.
@app.route("/index")
@app.route("/index.html")
# Sve rute se mapiraju na funkciju odmah ispod njih.
# Vezivanje ruta za funkciju realizovano je upotrebom dekoratora.
def forma_post():
    # Obradjivac zahteva kao odgovor na zahtev sada salje sadrzaj datoteka
    # forma_post.html. Funkcija send_file prima putanju do datoteke ciji sadrzaj je potrebno
    # poslati kao odgovor. Putanja je relativna u odnosu na putanju sa koje se
    # izvrsava skripta.
    return flask.send_file("forma_post.html")

# Ukoliko je zahtev upucen na URL /registracija i pri tome
# je tip zahteva GET ili POST funkcija registracija ce
# vrsiti obradu zahteva.
@app.route("/registracija", methods=["GET", "POST"])
def registracija():
    # Podaci o upucenom zahtevom sadrze se u request objektu.
    # Atribut method iz request objekta sadrzi string koji opisuje tip
    # upucenog zahteva.
    if flask.request.method == "GET":
        # Ukoliko je zahtev tipa GET vrednosti iz forme se mogu proslediti
        # samo kao argumenti URL-a. Vrednosti prosledjene iz forme nalaze
        # se u request objektu u atributu args. Atribut args predstavlja
        # imutabilni recnik, odnosno recnik cije vrednosti nije moguce menjati.
        return str(flask.request.args)
    elif flask.request.method == "POST":
        # Ukoliko je zahtev tipa POST vrednosti iz forme se prosledjuju
        # u telu zahteva i samim tim nisu vidljive u URL-u. Podaci iz forme
        # uskladisteni su u objektu request u atributu form. Atribut form
        # predstavlja imutabilni recnik, odnosno recnik cije vrednosti nije
        # moguce menjati.
        return str(flask.request.form)

# Aplikacija se pokrece tako da joj je moguce pristupa
# od "spolja", odnosno sa udaljenih racunara. Ovo je postignuto
# postavljanjem vrednosti prvog argumenta na ip adresu 0.0.0.0.
# Potom je, narednim argumentom, navedeno da aplikacija slusa na port 5000.
# Postavljanje vrednosti imenovanog argumenta threaded na True dovodi do toga
# da ce se svaki zahtev obradjivati u odvojenom thread-u.
# To znaci da prilikom obrade jednog zahteva nece doci do blokiranja
# izvrsavanja cele aplikacije ili obrade drugih zahteva.
app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.