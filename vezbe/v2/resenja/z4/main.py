import flask
from flask import Flask

# Kreira se instanca flask aplikacije.
app = Flask(__name__)

# Definisan je obradjivac zahteva upucenih na korenski URL.
@app.route("/")
def index_page():
    # Rezultat obrade zahteva je string "Uspešno obrađen zahtev!"
    # koji ce kao odgovor biti poslat klijentu.
    return "Uspešno obrađen zahtev!"

# Aplikacija se pokrece tako da joj je moguce pristupa
# od "spolja", odnosno sa udaljenih racunara. Ovo je postignuto
# postavljanjem vrednosti prvog argumenta na ip adresu 0.0.0.0.
# Potom je, narednim argumentom, navedeno da aplikacija slusa na port 5000.
# Postavljanje vrednosti imenovanog argumenta threaded na True dovodi do toga
# da ce se svaki zahtev obradjivati u odvojenom thread-u.
# To znaci da prilikom obrade jednog zahteva nece doci do blokiranja
# izvrsavanja cele aplikacije ili obrade drugih zahteva.
if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.