/* 
 * Posto je opseg najmanji vidljivosti promenljivih u JavaScript-u
 * funkcija izolovanje promenljivih u lokalne opsege se mora vrsiti
 * definisanjem funkcije. U ovom slucaju definisana je anonimna funkcija
 * koja je odmah nakon definicije i pozvana. Ovakav tip funkcija se u
 * JavaScript jeziku naziva IIFE sto je skracenica od Immediately Invoked
 * Function Expression.
 */
(function(d) { //Funkcija prima jedan parametar.
    var drodjenja = d.getElementById("drodjenja"); //Dobavljanje elemnta za odabir datuma.

    var validacijaDatuma = function(event) {
        var datum = new Date(event.target.value); //Objekat event se prosledjuje obradjivacu dogadajaj.
                                                  //Ovaj objekat sadrzi sve podatke o dogadjaju
                                                  //ukljucujuci i podatke o elementu koji je ciljan
                                                  //ovim dogadjajem, atribut target.
        var starost = new Date() - datum; //Razlika trenutnog datuma i vremena i zadatog datuma predstavlja
                                          //starost korisnika. Rezultat oduzimanja datuma bice unix timestamp,
                                          //Odnosno vreme proteklo od 1970. godine izrazeno u  milisekundama.
        starost = new Date(starost).getFullYear() - 1970; //Na osnovu timestamp-a se ponovo kreira objekat datuma
                                                          //kako bi se izracunala starost korisnika u godinama.
                                                          //Starost korisnika u godinama se racuna kao razlika
                                                          //izmedju broja godina dobijenih na osnovu timestamp-a i
                                                          //i broja 1970.
        if(starost >= 18) { //Provera da li je osoba koja se registruje punoletna.
            event.target.setCustomValidity(""); //Ukoliko je osoba punoletna podaci su ispravni i nema poruke
                                                //o gresci.
        } else {
            //Ukoliko je osoba mladja od 18 godina ispisuje se poruka zadata funkcijom
            //setCustomValidity i sprecava se prosledjivannje podataka forme.
            event.target.setCustomValidity("Registracija je dozvoljena samo punoletnim osobama.");
        }
    }

    drodjenja.onchange = validacijaDatuma; //Callback funkcija za promenu vrednosti bice funkcija validacijaDatuma.
})(document); //Funkciji je prosledjena globalna promenljiva document.