(function(angular){
    // Dobavljanje postojeceg modula app.
    var app = angular.module("app");

    // Kreiranje kontrolera pod nazivom ProizvodiCtrl.
    // Ovaj kontroler zavisi od servisa $http. Zavisnosti
    // se navode kao spisak stringova koji sadrze nazive
    // zavisnosti. Umetanje zavisnosti vrsi se preko parametara
    // funkcije koja predstavlja implementaciju kontrolera.
    // Nazivi parametara mogu biti proizvoljni.
    // Razlog za ovakav nacin umetanja zavisnosti je sto se
    // prilikom minifikacije stringovi ne menjaju pa ce se
    // samim tim sacuvati nazivi zavisnosti. Da su zavisnosti
    // navedene samo kao parametri funkcije, tokom minifikacije,
    // nazivi parametara bi se promenili pa nazivi navedenih
    // zavisnosti ne bi odgovarali nazivima zadatih zavisnosti.
    app.controller("ProizvodiCtrl", ["$http", function($http) {
        var that = this; // Neophodno je primenovati this
                         // kako bi se promenljiva this mogla
                         // koristiti u ugnjezdenim funkcijama.

        this.proizvodi = []; // Inicijalno proizvodi nisu dobavljeni.
        this.kategorije = []; // Inicijalno kategorije nisu dobavljene.
        // Inicijalno podaci o novom proizvodu su popunjeni podrazumevanim vrednostima.
        this.noviProizvod = {
            "naziv": "",
            "opis": "",
            "cena": 0,
            "kategorija": null,
            "kolicina": 0
        };

        // Funkcija za dobavljanje svih kategorija.
        this.dobaviKategorije = function() {
            // Upucuje se get zahtev na relativni URL api/kategorije.
            $http.get("api/kategorije").then(function(result){
                // Ukoliko server uspesno obradi zahtev i vrati odgovor
                // rezultat se nalazi u promenljivoj result.
                console.log(result);
                // Isparsirano telo odgovora je smesteno u promenljivu data
                // u result objektu.
                that.kategorije = result.data;
                // Incijalizacija kategorije u novom proizvodu
                // na prvu dobavljenu kategoriju.
                that.noviProizvod.kategorija = that.kategorije[0].id;
            },
            function(reason) {
                // Ukoliko je zahtev neuspesno obradjen, tj.
                // promise objekat nije uspesno razresen opis
                // greske, odnosno razlog za neuspeh je smesten u
                // promenljivu reason.
                console.log(reason);
            });
        }
        
        // Funkcija za dobavljanje proizvoda.
        this.dobaviProizvode = function() {
            // Upucuje se get zahtev na relativni URL api/proizvodi.
            $http.get("api/proizvodi").then(function(result){
                console.log(result);
                that.proizvodi = result.data;
            },
            function(reason) {
                console.log(reason);
            });
        }

        // Funkcija za dodavanje proizvoda.
        this.dodajProizvod = function() {
            // Upucuje se post zahtev na relativni URL api/proizvodi.
            // Uz url funkciji post se prosledjuje i objekat koji
            // treba da se prosledi u telu zahteva. Angular ce automatski
            // ovaj objekat pretvoriti u JSON format i tako ga proslediti
            // u telu zahteva.
            $http.post("api/proizvodi", this.noviProizvod).then(function(response) {
                console.log(response);
                // Pri uspesnom dodavanju neophodno je azurirati listu
                // proizvoda. Moguce je samo dodati kopiju proizvoda
                // u listu proizvoda:
                // that.proizvodi.push(angular.copy(that.noviProizvod));
                // Pri cemu se, ukoliko postoji sortiranje i/ili filtriranje proizvoda,
                // mora paziti na redosled dodavanja novog proizvoda.

                // Umesto toga moguce je ponovo uputiti zahtev serveru za dobavljanje
                // svih proizvoda sto dodatno opterecuje server ali pojednostavljuje
                // poslovnu logiku ukoliko je sortiranje i filtriranje implementirano
                // na serveru.
                that.dobaviProizvode();
            },
            function(reason) {
                console.log(reason);
            });
        }

        // Funkcija za uklanjanje proizvoda.
        this.ukloniProizvod = function(id) {
            // Pri uklanjanju proizvoda serveru se salje delete zahtev
            // na url api/proizvodi/<id> pri cemu id zavisi od proizvoda
            // koji je neophodno obrisati.
            $http.delete("api/proizvodi/" + id).then(function(response){
                console.log(response);
                that.dobaviProizvode();
            },
            function(reason){
                console.log(reason);
            });
        }

        this.dobaviKategorije();
        this.dobaviProizvode();
    }]);
})(angular);