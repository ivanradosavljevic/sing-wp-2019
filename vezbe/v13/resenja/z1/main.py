import flask
import datetime
from flask import Flask

# Dobavljanje mysql promenljive iz db modula.
from utils.db import mysql

# Dobavljanje blueprint-ova.
from blueprints.kategorije import kategorije_blueprint
from blueprints.proizvodi import proizvodi_blueprint
from blueprints.korisnici import korisnici_blueprint
from blueprints.login import login_blueprint

# Kreira se instanca flask aplikacije.
# Argument static_url_path odredjuje prefiks URL-a na koji ce se mapirati
# sadrzaj direktorijuma static. Podrazumevano je static. U ovom slucaju
# podeseno je da se mapira na korenski URL. To znaci da se zarad pristupanja
# statickim datotekama na serveru ne mora kucati URL localhost:5000/static/ime_datoteke
# vec da je dovoljno uneti localhost:5000/ime_datoteke. Sav staticki sadrzaj
# i dalje mora biti u direktorijumu static.
app = Flask(__name__, static_url_path="")

#Podesavanje konfiguracije za povezivanje na bazu podataka.
app.config["MYSQL_DATABASE_USER"] = "root" # Korisnicko ime.
app.config["MYSQL_DATABASE_PASSWORD"] = "root" # Lozinka.
app.config["MYSQL_DATABASE_DB"] = "prodavnica" # Naziv seme baze podataka na koju se treba povezati
app.config["SECRET_KEY"] = "ta WJoir29$" # Podesavanje tajnog kljuca koji se koristi za enkripciju kolacica.
                                         # Ovaj string je pozeljno izgenerisati nasumicno pri pokretanju
                                         # aplikacije.

mysql.init_app(app) # Naknadno prosledjivanje instance aplikacije
                    # za konfigurisanje objekta za upravljanje
                    # konekcijama ka bazi podataka.

# Registrovanje blueprint-a.
app.register_blueprint(kategorije_blueprint, url_prefix="/api")
app.register_blueprint(proizvodi_blueprint, url_prefix="/api")
app.register_blueprint(korisnici_blueprint, url_prefix="/api")
app.register_blueprint(login_blueprint, url_prefix="/api")

# Definisan je obradjivac zahteva upucenih na korenski URL.
# Moguce je navesti vise ruta za jedan obradjivac zahteva.
@app.route("/")
@app.route("/index")
# Sve rute se mapiraju na funkciju odmah ispod njih.
# Vezivanje ruta za funkciju realizovano je upotrebom dekoratora.
def index_page():
    # Kada korisnik pristupi bilo kojoj od navedenih
    # ruta bice mu prikazana index.html stranica.
    return app.send_static_file("index.html")

# Kako se sada aplikacija pokrece u odvojenom
# kontejneru a ne direktno kao python skritpa
# neophodno je dodati proveru da li je modul
# pokrenut direktno preko interpretera. Ukoliko
# je modul pokrenut direktno vrednost promenljive
# __name__ ce biti string __main__ pa ce se
# aplikacija pokrenuti pozivom metode run nad
# instancom klase Flask. Ukoliko je modul
# pokrenut iz kontejnera ovaj uslov nece biti
# zadovoljen pa se ni poziv metode run nece desiti
# nego ce kontejner izvrsiti pokretanje Flask aplikacije
# onako kako je to u izabranom kontejneru predvidjeno.
if __name__ == "__main__":
    # Aplikacija se pokrece tako da joj je moguce pristupa
    # od "spolja", odnosno sa udaljenih racunara. Ovo je postignuto
    # postavljanjem vrednosti prvog argumenta na ip adresu 0.0.0.0.
    # Potom je, narednim argumentom, navedeno da aplikacija slusa na port 5000.
    # Postavljanje vrednosti imenovanog argumenta threaded na True dovodi do toga
    # da ce se svaki zahtev obradjivati u odvojenom thread-u.
    # To znaci da prilikom obrade jednog zahteva nece doci do blokiranja
    # izvrsavanja cele aplikacije ili obrade drugih zahteva.
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.