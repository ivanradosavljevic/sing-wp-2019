import flask
from flask.blueprints import Blueprint

from utils.db import mysql

# Blueprint predstavlja komponentu aplikacije koja se
# moze koristiti vise puta. Sluzi za jednostavniju i
# jasniju organizaciju aplikacija u logicke celine.
# Blueprint se instancira kao i aplikacija.
# Prvo se navodi naziv blueprinta, a potom naziv
# modula u kojem se blueprint nalazi.
proizvodi_blueprint = Blueprint("proizvodi_blueprint", __name__)

@proizvodi_blueprint.route("/proizvodi", methods=["GET"])
def dobavi_proizvode():
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM proizvod WHERE kolicina > 0") # Izvrsavanje upita za dobavljanje svih proizvoda
                                                                # cija kolicina je veca od 0. Ovime je posao filtriranja
                                                                # proizvoda po kolicini izmesten iz generisanja sablona
                                                                # na server baze podataka koji je optimivan za ovakve
                                                                # zadatke.
    proizvodi = cursor.fetchall() # Dobavljanje svih rezultat prethodni izvrsenog upita.
    return flask.jsonify(proizvodi) # Vracanje proizvoda u JSON formatu.
                                    # Funkcija jsonify pretvara prosledjeni objekat u JSON format
                                    # i pakuje ga u response objekat u kojem su podesena sva
                                    # neophodna zaglavlja. Upotreba dumps funkcije iz
                                    # JSON modula vratila bi JSON ali ne bi napravila i response
                                    # objekat, koji bi se morao napraviti naknadno rucno.

@proizvodi_blueprint.route("/proizvodi/<int:id_proizvoda>")
def dobavi_proizvod(id_proizvoda, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM proizvod WHERE id=%s", (id_proizvoda,))
    proizvod = cursor.fetchone() # Kako je rezultat upita samo jedna torka dovoljno je
                                 # pozvati metodu fetchone() nad kursorom koja vraca
                                 # sledecu torku koja zadovoljava upit.
    if proizvod is not None:
        return flask.jsonify(proizvod) # Ukoliko je pronadje, proizvod se prosledjuje klijentu.
    else:
        return "", 404 # Ukoliko proizvod nije pronadjen klijent ce dobiti status odgovora 404.
                       # Odnosno podatak da trazeni resurs nije pronadjen.

@proizvodi_blueprint.route("/proizvodi", methods=["POST"])
def dodaj_proizvod():
    # Provera da li je korisnik prijavljen.
    if flask.session.get("korisnik") is None:
        return "", 403 # Ukoliko korisnik nije prijavljne vraca se statusno kod 403 forbidden.

    db = mysql.get_db() # Dobavljanje instance konekcije ka bazi.
    cursor = db.cursor() # Dobavljanje kursora.

    # Izvrsava se parametrizovani upit sa imenovanim parametrima. Ukoliko se koriste imenovani parametri
    # umesto torkse sa podacima moguce je proslediti recnik koji u se bi sadrzi kljuceve koji odgovaraju
    # imenima parametara. Vrednosti na datim kljucevima ce automatski biti preuzete u istoimenim parametrima.
    # Kako se sadrzaj forme u Flasku predstavlja kao imutabilni recnik koji nasledjuje recnik moguce je umesto
    # obicnog recnika proslediti sam sadrzaj forme. Takodje konverzija podatak nije neophodna jer ce se ispravna
    # konverzija izvrsiti na serveru za upravljanje bazama podataka.
    # Dodatna napomena: Iako je id kolona koja postoji u tabeli proizvod, ona nije navedena prilikom dodavanja
    # jer je ova kolona podesena da bude auto increment, odnosno da se njena vrednost moze automatski generisati.
    # Ovo generisanje ce se desiti samo ukoliko se prilikom izvrsavanja insert naredbe izostavi podesavanje vrednosti
    # za kolonu koja je auto increment ili ako se za njenu vrednost postavi NULL vrednost.
    cursor.execute("INSERT INTO proizvod(naziv, opis, kolicina, cena, kategorija_id) VALUES(%(naziv)s, %(opis)s, %(kolicina)s, %(cena)s, %(kategorija)s)", flask.request.json)
    # Request objekat u flasku sadrzi atribut json, ovaj atribut sadrzace recnik koji je nastao
    # parsiranjem tela zahteva. Telo ce biti parsirano ukoliko je content type bio application/json
    # a recnik ce biti formiran samo ukoliko se u telu nalazio ispravan JSON dokument.
    db.commit() # Upiti se izvrsavaju u transakcijama. Uskladistavanje rezultata transakcije je neophodno rucno potvrditi.
                # Za to se koristi commit() metoda nad konekcijom. Ukoliko se ne pozove commit() transakcija nece biti
                # potvrdjena pa se samim tim rezultat nece uskladistiti u bazu podataka. Vise upita koji zavise
                # jedan od drugo moguce je grupisati u jednu transakciju tako sto se nakon izvrsavanja cele grupe
                # upita pozove commit().
    return flask.jsonify(flask.request.json), 201 # Status kod 201 oznacava uspesno kreiran resurs.

@proizvodi_blueprint.route("/proizvodi/<int:id_proizvoda>", methods=["PUT"])
def izmeni_proizvod(id_proizvoda):
    # Provera da li je korisnik prijavljen.
    if flask.session.get("korisnik") is None:
        return "", 403 # Ukoliko korisnik nije prijavljne vraca se statusno kod 403 forbidden.
    
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_proizvoda # Id proizvoda koji treba azurirati preuzima
                              # se iz vrednosti parametra URL-a.
    cursor.execute("UPDATE proizvod SET naziv=%(naziv)s, opis=%(opis)s, cena=%(cena)s, kolicina=%(kolicina)s, kategorija_id=%(kategorija)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

# Uklanjanje proizvoda vrsi se po id-ju proizvoda.
# Id ce biti prosledjen kao parametar URL-a.
@proizvodi_blueprint.route("/proizvodi/<int:id_proizvoda>", methods=["DELETE"])
def ukloni_proizvod(id_proizvoda):
    # Provera da li je korisnik prijavljen.
    if flask.session.get("korisnik") is None:
        return "", 403 # Ukoliko korisnik nije prijavljne vraca se statusno kod 403 forbidden.
    
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM proizvod WHERE id=%s", (id_proizvoda,))
    db.commit()
    return "", 204 # Operacija je uspesna ali je telo odgovora prazno.