import flask
from flask.blueprints import Blueprint

from utils.db import mysql

kategorije_blueprint = Blueprint("kategorije_blueprint", __name__)

# Domaci zadatak: napisati preostale CRUD operacije za kategorije.

@kategorije_blueprint.route("/kategorije", methods=["GET"])
def dobavi_kategorije():
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM kategorija") # Izvrsavanje upita za dobavljanje svih kategorija.
    kategorije = cursor.fetchall() # Dobavljanje svih rezultat prethodni izvrsenog upita.
    return flask.jsonify(kategorije)