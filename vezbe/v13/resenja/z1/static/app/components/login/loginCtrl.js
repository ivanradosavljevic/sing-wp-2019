(function(angular){
    var app = angular.module("app");

    app.controller("loginCtrl", ["$http", function($http){
        var that = this;
        this.status = "neprijavljen"; // Pocetni status prijave.
        this.korisnik = {
            korisnicko_ime: "",
            lozinka: ""
        }

        this.login = function() {
            $http.post("api/login", this.korisnik).then(
                function(response) {
                    that.status = "prijavljen";
                    // Praznjenje polja sa podacima korisnika.
                    that.korisnik = {
                        korisnicko_ime: "",
                        lozinka: ""
                    }
                },
                function(reason) {
                    that.status = "neuspesno";
                    // Praznjenje polja sa podacima korisnika.
                    that.korisnik = {
                        korisnicko_ime: "",
                        lozinka: ""
                    }
                }
            )
        }

        this.logout = function() {
            $http.get("api/logout").then(
                function(response) {
                    that.status = "neprijavljen";
                },
                function(reason) {
                    console.log(reason);
                }
            )
        }

        this.getCurrentUser = function() {
            $http.get("api/currentUser").then(
                function(response) {
                    if(response.data) {
                        that.status = "prijavljen";
                    } else {
                        that.status = "neprijavljen";
                    }
                },
                function(reason) {
                    console.log(reason);
                }
            )
        }

        this.getCurrentUser(); // Provera da li je korisnik vec prijavljen.
    }]);
})(angular);