(function(angular){
    var app = angular.module("app");
    // U kontroler se pored $http servisa umece i $stateParams servis.
    // Servis $stateParams se koristi za pristup vrednostima parametara
    // parametrizovanih URL-ova. Vrednostima se pristupa preko kljuca
    // pri cemu kljuc odogavara imenu parametra navdenom prilikom
    // konfigurisanja stanja ui router komponente.
    app.controller("ProizvodCtrl", ["$http", "$stateParams", "$state", function($http, $stateParams, $state) {
        var that = this;
        this.proizvod = {};

        this.dobaviProizvod = function(id) {
            $http.get("api/proizvodi/" + id).then(function(result){
                that.proizvod = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        // Iz $stateParams se preuzima vrednost na kljucu id, koja odgovara
        // vrednosti primarnog kljuca trazenog proizvoda.
        this.dobaviProizvod($stateParams["id"]);
    }]);
})(angular)