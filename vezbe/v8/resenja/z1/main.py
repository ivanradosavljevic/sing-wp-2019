import flask
import datetime
from flask import Flask

from flaskext.mysql import MySQL # Importovanje klase za rad sa MySQL serverom.
from flaskext.mysql import pymysql # Importovanje paketa za podesavanje tipa kursora.

# Kreira se instanca flask aplikacije.
# Argument static_url_path odredjuje prefiks URL-a na koji ce se mapirati
# sadrzaj direktorijuma static. Podrazumevano je static. U ovom slucaju
# podeseno je da se mapira na korenski URL. To znaci da se zarad pristupanja
# statickim datotekama na serveru ne mora kucati URL localhost:5000/static/ime_datoteke
# vec da je dovoljno uneti localhost:5000/ime_datoteke. Sav staticki sadrzaj
# i dalje mora biti u direktorijumu static.
app = Flask(__name__, static_url_path="")

#Podesavanje konfiguracije za povezivanje na bazu podataka.
app.config["MYSQL_DATABASE_USER"] = "root" # Korisnicko ime.
app.config["MYSQL_DATABASE_PASSWORD"] = "root" # Lozinka.
app.config["MYSQL_DATABASE_DB"] = "prodavnica" # Naziv seme baze podataka na koju se treba povezati

# Instanciranje objekta za upravljanje konekcijama sa bazom podataka.
mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor) # Potrebno je proslediti flask aplikaciju iz koje
                                                           # je moguce procitati konfiguraciju, u ovom slucaju
                                                           # to je app instanca flask aplikacije.
                                                           # Argument cursorclass se koristi za podesavanje tipa
                                                           # kursora. U ovom slucaju kurosr je podesen da bude
                                                           # DictCursor sto znaci da ce rezultati upita biti vracani
                                                           # kao liste recnika ciju kljucevi su nazivi kolona a vrednosti
                                                           # vrednosti u datim kolonama. Ukliko se izostavi podesavanje
                                                           # za tip kursora koristi se podrazumevani tip kursora koji
                                                           # vraca torke torki u kojima se, redom navedenim u upitu
                                                           # nalaze vrednosti koje su rezultat upita.

# Definisan je obradjivac zahteva upucenih na korenski URL.
# Moguce je navesti vise ruta za jedan obradjivac zahteva.
@app.route("/")
@app.route("/index")
@app.route("/index.html")
# Sve rute se mapiraju na funkciju odmah ispod njih.
# Vezivanje ruta za funkciju realizovano je upotrebom dekoratora.
def index_page():
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM proizvod WHERE kolicina > 0") # Izvrsavanje upita za dobavljanje svih proizvoda
                                                                # cija kolicina je veca od 0. Ovime je posao filtriranja
                                                                # proizvoda po kolicini izmesten iz generisanja sablona
                                                                # na server baze podataka koji je optimivan za ovakve
                                                                # zadatke.
    proizvodi = cursor.fetchall() # Dobavljanje svih rezultat prethodni izvrsenog upita.

    # Fukncija render template kao argumente prima putanju do sablona
    # na osnovu kojeg se generise sadrzaj i promenljive koje ce biti
    # upotrebljene za generisanje sadrzaja. Promenljive se prosledjuju
    # kao keyword argumenti, odnoso parovi kljuc vrednost u formatu
    # kljuc=vrednost. Pored prosledjenih promenljivih pri generisanju
    # sadrzaja dostupne se u i ugradjene promenljive poput promenljive
    # request. Putanja do sablona je relativna u odnosu na putanju
    # zadatu argumentom template_folder pri konstruisanju instance klase Flask.
    # Podrazumevana vrednost template_folder argumenta je templates
    # sto znaci da svi sabloni treba da se nalaze u direktorijumu
    # templates.
    # Obradjivac zahteva kao odgovor na zahtev sada salje
    # sadrzaj generisan na osnovu sablona index.tpl.html.
    return flask.render_template("index.tpl.html", proizvodi=proizvodi)

@app.route("/dodavanje.html")
def dodavanje_proizvoda():
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM kategorija") # Izvrsavanje upita za dobavljanje svih kategorija.
    kategorije = cursor.fetchall() # Dobavljanje svih rezultat prethodni izvrsenog upita.
    # Vraca se sadrzaj koji je izgenerisan na osnovu sablona za prikaz
    # svih proizvoda.
    return flask.render_template("dodavanje.tpl.html", kategorije=kategorije)

# Ukoliko je zahtev upucen na URL /proizvodi i pri tome
# je tip zahteva GET funkcija prikaz_svih_proizvoda ce vrsiti obradu
# zahteva.
@app.route("/proizvodi", methods=["GET"])
def prikaz_svih_proizvoda():
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM proizvod") # Izvrsavanje upita za dobavljanje svih proizvoda.
    proizvodi = cursor.fetchall() # Dobavljanje svih rezultat prethodni izvrsenog upita.
    # Vraca se sadrzaj koji je izgenerisan na osnovu sablona za prikaz
    # svih proizvoda.
    return flask.render_template("proizvodi.tpl.html", proizvodi=proizvodi)

# Ukoliko je zahtev upucen na URL /proizvodi/INDEKS_PROIZVODA i pri tome
# je tip zahteva GET funkcija priakz_proizvoda ce vrsiti obradu
# zahteva. Ruta je parametrizovana tako da moze primiti proizvoljan
# indeks proizvoda.
@app.route("/proizvodi/<int:proizvod_id>", methods=["GET"])
def priakz_proizvoda(proizvod_id): # Prosledjivace se id proizvoda iz baze podataka a ne indeks proizvoda u petlji.
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM proizvod WHERE id=%s", (proizvod_id, )) # Izvrsavanje upita za dobavljanje proizvoda
                                                                          # sa zadatim id-jem. Upit je parametrizovan
                                                                          # tako da se podaci prosledjuju odvojeno od
                                                                          # samog upita. Podaci se u ovom slucaju
                                                                          # prosledjuju kao torka podataka.
    proizvod = cursor.fetchone() # Kako je id jedinstven dobavljane proizvoda po id-ju moze vratiti maksimalno jedan
                                 # proizvod pa je zbog toga pozeljno koristiti metodu fetchone() koja ce kao rezultat
                                 # vratiti jednu torku ili recnik, u zavisnosti od tipa kursora, a ne torku torki ili
                                 # listu recnika kao metoda fetchall().
    # Vraca se sadrzaj koji je izgenerisan na osnovu sablona za prikaz jednog proizvoda.
    return flask.render_template("proizvod.tpl.html", proizvod=proizvod)

# Ukoliko je zahtev upucen na URL /proizvodi i pri tome
# je tip zahteva POST funkcija dodaj_proizvod ce vrsiti obradu
# zahteva.
@app.route("/proizvodi", methods=["POST"])
def dodaj_proizvod():
    db = mysql.get_db() # Dobavljanje instance konekcije ka bazi.
    cursor = db.cursor() # Dobavljanje kursora.

    # Izvrsava se parametrizovani upit sa imenovanim parametrima. Ukoliko se koriste imenovani parametri
    # umesto torkse sa podacima moguce je proslediti recnik koji u se bi sadrzi kljuceve koji odgovaraju
    # imenima parametara. Vrednosti na datim kljucevima ce automatski biti preuzete u istoimenim parametrima.
    # Kako se sadrzaj forme u Flasku predstavlja kao imutabilni recnik koji nasledjuje recnik moguce je umesto
    # obicnog recnika proslediti sam sadrzaj forme. Takodje konverzija podatak nije neophodna jer ce se ispravna
    # konverzija izvrsiti na serveru za upravljanje bazama podataka.
    # Dodatna napomena: Iako je id kolona koja postoji u tabeli proizvod, ona nije navedena prilikom dodavanja
    # jer je ova kolona podesena da bude auto increment, odnosno da se njena vrednost moze automatski generisati.
    # Ovo generisanje ce se desiti samo ukoliko se prilikom izvrsavanja insert naredbe izostavi podesavanje vrednosti
    # za kolonu koja je auto increment ili ako se za njenu vrednost postavi NULL vrednost.
    cursor.execute("INSERT INTO proizvod(naziv, opis, kolicina, cena, kategorija_id) VALUES(%(naziv)s, %(opis)s, %(kolicina)s, %(cena)s, %(kategorija)s)", flask.request.form)
    db.commit() # Upiti se izvrsavaju u transakcijama. Uskladistavanje rezultata transakcije je neophodno rucno potvrditi.
                # Za to se koristi commit() metoda nad konekcijom. Ukoliko se ne pozove commit() transakcija nece biti
                # potvrdjena pa se samim tim rezultat nece uskladistiti u bazu podataka. Vise upita koji zavise
                # jedan od drugo moguce je grupisati u jednu transakciju tako sto se nakon izvrsavanja cele grupe
                # upita pozove commit().

    # Fukncija redirect kao argument prima URL do resursa na koji
    # je neophodno preusmeriti zahtev. U ovom slucaju preusmeravanje
    # se vrsi na stranicu proizvoda kako bi se nakon dodavanja proizvoda
    # prikazala tabela svih proizvoda.
    return flask.redirect("/proizvodi")

# Ukoliko je zahtev upucen na URL /proizvodi/izmena/INDEKS_PROIZVODA i pri tome
# je tip zahteva GET funkcija izmeni_proizvod_forma ce vrsiti obradu
# zahteva. Ruta je parametrizovana tako da moze primiti proizvoljan
# indeks proizvoda.
@app.route("/proizvodi/izmena/<int:proizvod_id>", methods=["GET"])
def izmeni_proizvod_forma(proizvod_id):
    # Radi ispravnog popunjavanja forme za izmenu proizvoda neophodno je dobaviti sve kategorije i proizvod koji
    # je potrebno izmeniti.

    #Dobavljanje kategorija.
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM kategorija") # Izvrsavanje upita za dobavljanje svih kategorija.
    kategorije = cursor.fetchall() # Dobavljanje svih rezultat prethodni izvrsenog upita.

    # Dobavljanje proizvoda.
    # Nije neophodno dobavljati novi cursor jer je cursor vec ranije dobavljen.
    cursor.execute("SELECT * FROM proizvod WHERE id=%s", (proizvod_id, )) # Izvrsavanje upita za dobavljanje proizvoda
                                                                          # sa zadatim id-jem. Upit je parametrizovan
                                                                          # tako da se podaci prosledjuju odvojeno od
                                                                          # samog upita. Podaci se u ovom slucaju
                                                                          # prosledjuju kao torka podataka.
    proizvod = cursor.fetchone() # Kako je id jedinstven dobavljane proizvoda po id-ju moze vratiti maksimalno jedan
                                 # proizvod pa je zbog toga pozeljno koristiti metodu fetchone() koja ce kao rezultat
                                 # vratiti jednu torku ili recnik, u zavisnosti od tipa kursora, a ne torku torki ili
                                 # listu recnika kao metoda fetchall().
    # Vraca se sadrzaj koji je izgenerisan na osnovu sablona za prikaz jednog proizvoda.
    # Vraca se sadrzaj koji je izgenerisan na osnovu sablona za prikaz forme za izmenu proizvoda.
    return flask.render_template("izmena.tpl.html", kategorije=kategorije, proizvod=proizvod)

# Ukoliko je zahtev upucen na URL /proizvodi/izmeni/INDEKS_PROIZVODA i pri tome
# je tip zahteva POST funkcija izmeni_proizvod ce vrsiti obradu
# zahteva. Ruta je parametrizovana tako da moze primiti proizvoljan
# indeks proizvoda.
@app.route("/proizvodi/izmeni/<int:proizvod_id>", methods=["POST"])
def izmeni_proizvod(proizvod_id):
    db = mysql.get_db() # Dobavljanje instance konekcije ka bazi.
    cursor = db.cursor() # Dobavljanje kursora.

    # U recnik koji se prosledjuje pri izmeni proizvoda je neophodno dodati i id proizvoda. Iz tog razloga se na osnovu
    # form recnika kreira novi mutabilni recnik u koji se dodaj novi kljuc id sa vrednoscu koja odgovara id-ju proizvoda.
    # Id je bilo moguce proslediti i u formi kao skriveno input polje sa vrednoscu zadatog id-ja.
    # Detalji za skrivan input polja se mogu naci na linku: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/hidden.
    podaci = dict(flask.request.form)
    podaci["id"] = proizvod_id

    # Parametrizovani upit za izemnu proizvoda sa zadatim id-jem.
    cursor.execute("UPDATE proizvod SET naziv=%(naziv)s, opis=%(opis)s, kolicina=%(kolicina)s, cena=%(cena)s, kategorija_id=%(kategorija)s WHERE id=%(id)s", podaci)
    db.commit() # Upiti se izvrsavaju u transakcijama. Uskladistavanje rezultata transakcije je neophodno rucno potvrditi.
                # Za to se koristi commit() metoda nad konekcijom. Ukoliko se ne pozove commit() transakcija nece biti
                # potvrdjena pa se samim tim rezultat nece uskladistiti u bazu podataka. Vise upita koji zavise
                # jedan od drugo moguce je grupisati u jednu transakciju tako sto se nakon izvrsavanja cele grupe
                # upita pozove commit().
    # Fukncija redirect kao argument prima URL do resursa na koji
    # je neophodno preusmeriti zahtev. U ovom slucaju preusmeravanje
    # se vrsi na stranicu proizvoda kako bi se nakon dodavanja proizvoda
    # prikazala tabela svih proizvoda.
    return flask.redirect("/proizvodi")

# Ukoliko je zahtev upucen na URL /proizvodi/ukloni/INDEKS_PROIZVODA i pri tome
# je tip zahteva GET funkcija ukloni_proizvod ce vrsiti obradu
# zahteva. Ruta je parametrizovana tako da moze primiti proizvoljan
# indeks proizvoda.
@app.route("/proizvodi/ukloni/<int:proizvod_id>", methods=["GET"])
def ukloni_proizvod(proizvod_id):
    db = mysql.get_db() # Dobavljanje instance konekcije ka bazi.
    cursor = db.cursor() # Dobavljanje kursora.
    # Parametrizovani upit za fizicko uklanjanje proizvoda sa zadatim id-jem.
    cursor.execute("DELETE FROM proizvod WHERE id=%s", (proizvod_id, ))
    db.commit() # Upiti se izvrsavaju u transakcijama. Uskladistavanje rezultata transakcije je neophodno rucno potvrditi.
                # Za to se koristi commit() metoda nad konekcijom. Ukoliko se ne pozove commit() transakcija nece biti
                # potvrdjena pa se samim tim rezultat nece uskladistiti u bazu podataka. Vise upita koji zavise
                # jedan od drugo moguce je grupisati u jednu transakciju tako sto se nakon izvrsavanja cele grupe
                # upita pozove commit().
    return flask.redirect("/proizvodi")

# Kako se sada aplikacija pokrece u odvojenom
# kontejneru a ne direktno kao python skritpa
# neophodno je dodati proveru da li je modul
# pokrenut direktno preko interpretera. Ukoliko
# je modul pokrenut direktno vrednost promenljive
# __name__ ce biti string __main__ pa ce se
# aplikacija pokrenuti pozivom metode run nad
# instancom klase Flask. Ukoliko je modul
# pokrenut iz kontejnera ovaj uslov nece biti
# zadovoljen pa se ni poziv metode run nece desiti
# nego ce kontejner izvrsiti pokretanje Flask aplikacije
# onako kako je to u izabranom kontejneru predvidjeno.
if __name__ == "__main__":
    # Aplikacija se pokrece tako da joj je moguce pristupa
    # od "spolja", odnosno sa udaljenih racunara. Ovo je postignuto
    # postavljanjem vrednosti prvog argumenta na ip adresu 0.0.0.0.
    # Potom je, narednim argumentom, navedeno da aplikacija slusa na port 5000.
    # Postavljanje vrednosti imenovanog argumenta threaded na True dovodi do toga
    # da ce se svaki zahtev obradjivati u odvojenom thread-u.
    # To znaci da prilikom obrade jednog zahteva nece doci do blokiranja
    # izvrsavanja cele aplikacije ili obrade drugih zahteva.
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.