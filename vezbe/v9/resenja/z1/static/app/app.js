(function(angular){
    // Kreiranje modula pod nazivom app koji nema dodatnih zavisnosti.
    // U slucaju da je modul vec kreiran, umesto kreiranja novog, bice
    // dobavljen postojeci modul.
    var app = angular.module("app", []);
})(angular);