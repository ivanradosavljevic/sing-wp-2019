import flask
import datetime
from flask import Flask

from flaskext.mysql import MySQL # Importovanje klase za rad sa MySQL serverom.
from flaskext.mysql import pymysql # Importovanje paketa za podesavanje tipa kursora.

# Kreira se instanca flask aplikacije.
# Argument static_url_path odredjuje prefiks URL-a na koji ce se mapirati
# sadrzaj direktorijuma static. Podrazumevano je static. U ovom slucaju
# podeseno je da se mapira na korenski URL. To znaci da se zarad pristupanja
# statickim datotekama na serveru ne mora kucati URL localhost:5000/static/ime_datoteke
# vec da je dovoljno uneti localhost:5000/ime_datoteke. Sav staticki sadrzaj
# i dalje mora biti u direktorijumu static.
app = Flask(__name__, static_url_path="")

#Podesavanje konfiguracije za povezivanje na bazu podataka.
app.config["MYSQL_DATABASE_USER"] = "root" # Korisnicko ime.
app.config["MYSQL_DATABASE_PASSWORD"] = "root" # Lozinka.
app.config["MYSQL_DATABASE_DB"] = "prodavnica" # Naziv seme baze podataka na koju se treba povezati

# Instanciranje objekta za upravljanje konekcijama sa bazom podataka.
mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor) # Potrebno je proslediti flask aplikaciju iz koje
                                                           # je moguce procitati konfiguraciju, u ovom slucaju
                                                           # to je app instanca flask aplikacije.
                                                           # Argument cursorclass se koristi za podesavanje tipa
                                                           # kursora. U ovom slucaju kurosr je podesen da bude
                                                           # DictCursor sto znaci da ce rezultati upita biti vracani
                                                           # kao liste recnika ciju kljucevi su nazivi kolona a vrednosti
                                                           # vrednosti u datim kolonama. Ukliko se izostavi podesavanje
                                                           # za tip kursora koristi se podrazumevani tip kursora koji
                                                           # vraca torke torki u kojima se, redom navedenim u upitu
                                                           # nalaze vrednosti koje su rezultat upita.

# Definisan je obradjivac zahteva upucenih na korenski URL.
# Moguce je navesti vise ruta za jedan obradjivac zahteva.
@app.route("/")
@app.route("/index")
# Sve rute se mapiraju na funkciju odmah ispod njih.
# Vezivanje ruta za funkciju realizovano je upotrebom dekoratora.
def index_page():
    # Kada korisnik pristupi bilo kojoj od navedenih
    # ruta bice mu prikazana index.html stranica.
    return app.send_static_file("index.html")

@app.route("/api/proizvodi", methods=["GET"])
def dobavi_proizvode():
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM proizvod WHERE kolicina > 0") # Izvrsavanje upita za dobavljanje svih proizvoda
                                                                # cija kolicina je veca od 0. Ovime je posao filtriranja
                                                                # proizvoda po kolicini izmesten iz generisanja sablona
                                                                # na server baze podataka koji je optimivan za ovakve
                                                                # zadatke.
    proizvodi = cursor.fetchall() # Dobavljanje svih rezultat prethodni izvrsenog upita.
    return flask.jsonify(proizvodi) # Vracanje proizvoda u JSON formatu.
                                    # Funkcija jsonify pretvara prosledjeni objekat u JSON format
                                    # i pakuje ga u response objekat u kojem su podesena sva
                                    # neophodna zaglavlja. Upotreba dumps funkcije iz
                                    # JSON modula vratila bi JSON ali ne bi napravila i response
                                    # objekat, koji bi se morao napraviti naknadno rucno.

@app.route("/api/proizvodi", methods=["POST"])
def dodaj_proizvod():
    db = mysql.get_db() # Dobavljanje instance konekcije ka bazi.
    cursor = db.cursor() # Dobavljanje kursora.

    # Izvrsava se parametrizovani upit sa imenovanim parametrima. Ukoliko se koriste imenovani parametri
    # umesto torkse sa podacima moguce je proslediti recnik koji u se bi sadrzi kljuceve koji odgovaraju
    # imenima parametara. Vrednosti na datim kljucevima ce automatski biti preuzete u istoimenim parametrima.
    # Kako se sadrzaj forme u Flasku predstavlja kao imutabilni recnik koji nasledjuje recnik moguce je umesto
    # obicnog recnika proslediti sam sadrzaj forme. Takodje konverzija podatak nije neophodna jer ce se ispravna
    # konverzija izvrsiti na serveru za upravljanje bazama podataka.
    # Dodatna napomena: Iako je id kolona koja postoji u tabeli proizvod, ona nije navedena prilikom dodavanja
    # jer je ova kolona podesena da bude auto increment, odnosno da se njena vrednost moze automatski generisati.
    # Ovo generisanje ce se desiti samo ukoliko se prilikom izvrsavanja insert naredbe izostavi podesavanje vrednosti
    # za kolonu koja je auto increment ili ako se za njenu vrednost postavi NULL vrednost.
    cursor.execute("INSERT INTO proizvod(naziv, opis, kolicina, cena, kategorija_id) VALUES(%(naziv)s, %(opis)s, %(kolicina)s, %(cena)s, %(kategorija)s)", flask.request.json)
    # Request objekat u flasku sadrzi atribut json, ovaj atribut sadrzace recnik koji je nastao
    # parsiranjem tela zahteva. Telo ce biti parsirano ukoliko je content type bio application/json
    # a recnik ce biti formiran samo ukoliko se u telu nalazio ispravan JSON dokument.
    db.commit() # Upiti se izvrsavaju u transakcijama. Uskladistavanje rezultata transakcije je neophodno rucno potvrditi.
                # Za to se koristi commit() metoda nad konekcijom. Ukoliko se ne pozove commit() transakcija nece biti
                # potvrdjena pa se samim tim rezultat nece uskladistiti u bazu podataka. Vise upita koji zavise
                # jedan od drugo moguce je grupisati u jednu transakciju tako sto se nakon izvrsavanja cele grupe
                # upita pozove commit().
    return flask.jsonify(flask.request.json), 201 # Status kod 201 oznacava uspesno kreiran resurs.

@app.route("/api/kategorije", methods=["GET"])
def dobavi_kategorije():
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM kategorija") # Izvrsavanje upita za dobavljanje svih kategorija.
    kategorije = cursor.fetchall() # Dobavljanje svih rezultat prethodni izvrsenog upita.
    return flask.jsonify(kategorije)

# Kako se sada aplikacija pokrece u odvojenom
# kontejneru a ne direktno kao python skritpa
# neophodno je dodati proveru da li je modul
# pokrenut direktno preko interpretera. Ukoliko
# je modul pokrenut direktno vrednost promenljive
# __name__ ce biti string __main__ pa ce se
# aplikacija pokrenuti pozivom metode run nad
# instancom klase Flask. Ukoliko je modul
# pokrenut iz kontejnera ovaj uslov nece biti
# zadovoljen pa se ni poziv metode run nece desiti
# nego ce kontejner izvrsiti pokretanje Flask aplikacije
# onako kako je to u izabranom kontejneru predvidjeno.
if __name__ == "__main__":
    # Aplikacija se pokrece tako da joj je moguce pristupa
    # od "spolja", odnosno sa udaljenih racunara. Ovo je postignuto
    # postavljanjem vrednosti prvog argumenta na ip adresu 0.0.0.0.
    # Potom je, narednim argumentom, navedeno da aplikacija slusa na port 5000.
    # Postavljanje vrednosti imenovanog argumenta threaded na True dovodi do toga
    # da ce se svaki zahtev obradjivati u odvojenom thread-u.
    # To znaci da prilikom obrade jednog zahteva nece doci do blokiranja
    # izvrsavanja cele aplikacije ili obrade drugih zahteva.
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.