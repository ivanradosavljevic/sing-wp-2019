(function(angular){
    // Kreiranje modula pod nazivom app koji kao zavisnost ima ui router modul.
    // U slucaju da je modul vec kreiran, umesto kreiranja novog, bice
    // dobavljen postojeci modul.
    var app = angular.module("app", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        // Stanje se registruje preko $stateProvider servisa, pozivom
        // metode state kojoj se prosledjuje state objekat.
        $stateProvider.state({
            name: "home", // Naziv stanja, mora biti zadat.
            url: "/", // URL kojim se prelazi na zadato stanje.
            templateUrl: "app/components/pocetna/pocetna.tpl.html", // URL do sablona koji se koristi za zadato stanje.
            controller: "PocetniCtrl", // Kontroler koji se vezuje za sablon u zadatom stanju.
            controllerAs: "pctrl" // Naziv vezanog kontrolera.
        }).state({
            name: "dodavanjeSpecijalizacije",
            url: "/specijalizacijaForma",
            templateUrl: "app/components/specijalizacijaForma/specijalizacijaForma.tpl.html",
            controller: "SpecijalizacijaFormaCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "specijalizacija",
            url: "/specijalizacija/{id}", // Ovaj URL je razlicit od URL-a za pristup svim proizvodima
                                    // jer sadrzi parametar id. Samo ukoliko se navede i parametar
                                    // dolazi do poklapanja sa ovim URL-om i do aktiviranja stanja.
            templateUrl: "app/components/specijalizacija/specijalizacija.tpl.html",
            controller: "SpecijalizacijaCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "novaSpecijalizacija",
            url: "/specijalizacija/", // Ovaj URL je razlicit od URL-a za pristup svim proizvodima
                                    // jer sadrzi parametar id. Samo ukoliko se navede i parametar
                                    // dolazi do poklapanja sa ovim URL-om i do aktiviranja stanja.
            templateUrl: "app/components/specijalizacija/specijalizacija.tpl.html",
            controller: "SpecijalizacijaCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "izmenaSpecijalizacije",
            url: "/specijalizacijaForma/{id}",
            templateUrl: "app/components/specijalizacijaForma/specijalizacijaForma.tpl.html",
            controller: "SpecijalizacijaFormaCtrl", // Isti kontroler se moze koristiti vise puta.
            controllerAs: "pctrl"
        }).state({
            name: "lekar",
            url: "/lekar/{id}", // Ovaj URL je razlicit od URL-a za pristup svim proizvodima
                                    // jer sadrzi parametar id. Samo ukoliko se navede i parametar
                                    // dolazi do poklapanja sa ovim URL-om i do aktiviranja stanja.
            templateUrl: "app/components/lekar/lekar.tpl.html",
            controller: "LekarCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "noviLekar",
            url: "/lekar/", // Ovaj URL je razlicit od URL-a za pristup svim proizvodima
                                    // jer sadrzi parametar id. Samo ukoliko se navede i parametar
                                    // dolazi do poklapanja sa ovim URL-om i do aktiviranja stanja.
            templateUrl: "app/components/lekar/lekar.tpl.html",
            controller: "LekarCtrl",
            controllerAs: "pctrl"
        });


        // Ukoliko zadati url ne pokazuje ni na jedno stanje
        // vrsi se preusmeravajne na url zadat pozivom metode
        // otherwise nad $urlRouterProvider servisom.
        $urlRouterProvider.otherwise("/")
    }])
})(angular);