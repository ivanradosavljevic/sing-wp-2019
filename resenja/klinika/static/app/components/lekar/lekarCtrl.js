(function(angular){
    var app = angular.module("app");
    // U kontroler se pored $http servisa umece i $stateParams servis.
    // Servis $stateParams se koristi za pristup vrednostima parametara
    // parametrizovanih URL-ova. Vrednostima se pristupa preko kljuca
    // pri cemu kljuc odogavara imenu parametra navdenom prilikom
    // konfigurisanja stanja ui router komponente.
    app.controller("LekarCtrl", ["$http", "$stateParams", "$state", function($http, $stateParams, $state) {
        var that = this;
        this.lekar = {};

        this.dobaviLekara = function(id) {
            $http.get("api/lekar/" + id).then(function(result){
                that.lekar = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        // Funkcija za dodavanje proizvoda.
        this.dodajLekara = function () {
            $http.post("api/lekar", this.lekar).then(function (response) {
                console.log(response);
                // Pri uspesnom dodavanju proizvoda precice se na stranicu za prikaz
                // svih proizvoda. Prelazak na stranicu moze se postici pozivom metode
                // go iz $state servisa. Ovoj metodi se prosledjuje naziv stanja na
                // koje je potrebno preci i po potrebi objekat koji sadrzi vrednosti
                // parametara koje treba proslediti pri prelasku u navedeno stanje.
                $state.go("home")
            }, function (reason) {
                console.log(reason);
            });
        }

        this.izmeniLekara = function(id) {
            $http.put("api/lekar/" + id, that.lekar).then(function(response) {
                console.log(response)
                // Prelazak na stanje sa parametrizovanim ULR-om pri cemu
                // se vrednost parametra prosledjuje prilikom poziva
                // metode go.
                $state.go("home");
            }, function(reason) {
                console.log(reason);
            });
        }

        this.sacuvaj = function() {
            if($stateParams["id"]) {
                this.izmeniLekara($stateParams["id"], that.lekar);
            } else {
                this.dodajLekara();
            }
        }


        // Iz $stateParams se preuzima vrednost na kljucu id, koja odgovara
        // vrednosti primarnog kljuca trazenog proizvoda.
        if($stateParams["id"]) {
            this.dobaviLekara($stateParams["id"]);
        }else{
            this.lekar = {
                aktivan:1
            };
        }
    }]);
})(angular)