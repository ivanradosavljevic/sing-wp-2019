(function (angular) {
    var app = angular.module("app");
    app.controller("ProizvodFormaCtrl", ["$http", "$state", "$stateParams", function ($http, $state, $stateParams) {
        var that = this;

        this.kategorije = []; // Inicijalno kategorije nisu dobavljene.
        // Inicijalno podaci o novom proizvodu su popunjeni podrazumevanim vrednostima.
        this.noviProizvod = {
            "naziv": "",
            "opis": "",
            "cena": 0,
            "kategorija": null,
            "kolicina": 0
        };

        // Funkcija za dobavljanje svih kategorija.
        this.dobaviKategorije = function() {
            // Upucuje se get zahtev na relativni URL api/kategorije.
            $http.get("api/kategorije").then(function(result){
                // Ukoliko server uspesno obradi zahtev i vrati odgovor
                // rezultat se nalazi u promenljivoj result.
                console.log(result);
                // Isparsirano telo odgovora je smesteno u promenljivu data
                // u result objektu.
                that.kategorije = result.data;
                // Incijalizacija kategorije u novom proizvodu
                // na prvu dobavljenu kategoriju.
                if(!$stateParams["id"]) {
                    that.noviProizvod.kategorija = that.kategorije[0].id;
                }
            },
            function(reason) {
                // Ukoliko je zahtev neuspesno obradjen, tj.
                // promise objekat nije uspesno razresen opis
                // greske, odnosno razlog za neuspeh je smesten u
                // promenljivu reason.
                console.log(reason);
            });
        }

        this.dobaviProizvod = function(id) {
            $http.get("api/proizvodi/" + id).then(function(result){
                that.noviProizvod = result.data;
                that.noviProizvod["kategorija"] = that.noviProizvod["kategorija_id"];
            }, function(reason) {
                console.log(reason);
            });
        }

        // Funkcija za dodavanje proizvoda.
        this.dodajProizvod = function () {
            // Upucuje se post zahtev na relativni URL api/proizvodi.
            // Uz url funkciji post se prosledjuje i objekat koji
            // treba da se prosledi u telu zahteva. Angular ce automatski
            // ovaj objekat pretvoriti u JSON format i tako ga proslediti
            // u telu zahteva.
            $http.post("api/proizvodi", this.noviProizvod).then(function (response) {
                console.log(response);
                // Pri uspesnom dodavanju proizvoda precice se na stranicu za prikaz
                // svih proizvoda. Prelazak na stranicu moze se postici pozivom metode
                // go iz $state servisa. Ovoj metodi se prosledjuje naziv stanja na
                // koje je potrebno preci i po potrebi objekat koji sadrzi vrednosti
                // parametara koje treba proslediti pri prelasku u navedeno stanje.
                $state.go("proizvodi")
            }, function (reason) {
                console.log(reason);
            });
        }

        this.izmeniProizvod = function(id) {
            $http.put("api/proizvodi/" + id, that.noviProizvod).then(function(response) {
                console.log(response)
                // Prelazak na stanje sa parametrizovanim ULR-om pri cemu
                // se vrednost parametra prosledjuje prilikom poziva
                // metode go.
                $state.go("proizvod", {"id": id});
            }, function(reason) {
                console.log(reason);
            });
        }

        this.sacuvaj = function() {
            if($stateParams["id"]) {
                this.izmeniProizvod($stateParams["id"], that.proizvod);
            } else {
                this.dodajProizvod();
            }
        }

        this.dobaviKategorije();
        if($stateParams["id"]) {
            this.dobaviProizvod($stateParams["id"]);
        }
    }]);
})(angular);