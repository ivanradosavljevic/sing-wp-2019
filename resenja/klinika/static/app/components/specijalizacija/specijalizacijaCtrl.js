(function(angular){
    var app = angular.module("app");
    // U kontroler se pored $http servisa umece i $stateParams servis.
    // Servis $stateParams se koristi za pristup vrednostima parametara
    // parametrizovanih URL-ova. Vrednostima se pristupa preko kljuca
    // pri cemu kljuc odogavara imenu parametra navdenom prilikom
    // konfigurisanja stanja ui router komponente.
    app.controller("SpecijalizacijaCtrl", ["$http", "$stateParams", "$state", function($http, $stateParams, $state) {
        var that = this;
        this.specijalizacija = {};

        this.dobaviSpecijalizaciju = function(id) {
            $http.get("api/specijalizacija/" + id).then(function(result){
                that.specijalizacija = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        // Funkcija za dodavanje proizvoda.
        this.dodajSpecijalizaciju = function () {
            $http.post("api/specijalizacija", this.specijalizacija).then(function (response) {
                console.log(response);
                // Pri uspesnom dodavanju proizvoda precice se na stranicu za prikaz
                // svih proizvoda. Prelazak na stranicu moze se postici pozivom metode
                // go iz $state servisa. Ovoj metodi se prosledjuje naziv stanja na
                // koje je potrebno preci i po potrebi objekat koji sadrzi vrednosti
                // parametara koje treba proslediti pri prelasku u navedeno stanje.
                $state.go("home")
            }, function (reason) {
                console.log(reason);
            });
        }

        this.izmeniSpecijalizaciju = function(id) {
            $http.put("api/specijalizacija/" + id, that.specijalizacija).then(function(response) {
                console.log(response)
                // Prelazak na stanje sa parametrizovanim ULR-om pri cemu
                // se vrednost parametra prosledjuje prilikom poziva
                // metode go.
                $state.go("home");
            }, function(reason) {
                console.log(reason);
            });
        }

        this.sacuvaj = function() {
            if($stateParams["id"]) {
                this.izmeniSpecijalizaciju($stateParams["id"], that.specijalizacija);
            } else {
                this.dodajSpecijalizaciju();
            }
        }


        // Iz $stateParams se preuzima vrednost na kljucu id, koja odgovara
        // vrednosti primarnog kljuca trazenog proizvoda.
        if($stateParams["id"]) {
            this.dobaviSpecijalizaciju($stateParams["id"]);
        }else{
            this.specijalizacija = {
                aktivan:1
            };
        }
    }]);
})(angular)