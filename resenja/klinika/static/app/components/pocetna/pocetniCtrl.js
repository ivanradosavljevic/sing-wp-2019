(function(angular){
    // Dobavljanje postojeceg modula app.
    var app = angular.module("app");

    // Kreiranje kontrolera pod nazivom ProizvodiCtrl.
    // Ovaj kontroler zavisi od servisa $http. Zavisnosti
    // se navode kao spisak stringova koji sadrze nazive
    // zavisnosti. Umetanje zavisnosti vrsi se preko parametara
    // funkcije koja predstavlja implementaciju kontrolera.
    // Nazivi parametara mogu biti proizvoljni.
    // Razlog za ovakav nacin umetanja zavisnosti je sto se
    // prilikom minifikacije stringovi ne menjaju pa ce se
    // samim tim sacuvati nazivi zavisnosti. Da su zavisnosti
    // navedene samo kao parametri funkcije, tokom minifikacije,
    // nazivi parametara bi se promenili pa nazivi navedenih
    // zavisnosti ne bi odgovarali nazivima zadatih zavisnosti.
    app.controller("PocetniCtrl", ["$http", function($http) {
        var that = this; // Neophodno je primenovati this
                         // kako bi se promenljiva this mogla
                         // koristiti u ugnjezdenim funkcijama.

        this.specijalizacije = []; // Inicijalno proizvodi nisu dobavljeni.
        this.lekari = [];

        // Funkcija za dobavljanje proizvoda.
        this.dobaviSpecijalizacije = function() {
            // Upucuje se get zahtev na relativni URL api/proizvodi.
            $http.get("api/specijalizacije").then(function(result){
                console.log(result);
                that.specijalizacije = result.data;
            },
            function(reason) {
                console.log(reason);
            });
        }

        // Funkcija za uklanjanje proizvoda.
        this.ukloniSpecijalizaciju = function(id) {
            // Pri uklanjanju proizvoda serveru se salje delete zahtev
            // na url api/proizvodi/<id> pri cemu id zavisi od proizvoda
            // koji je neophodno obrisati.
            $http.delete("api/specijalizacija/" + id).then(function(response){
                console.log(response);
                that.dobaviSpecijalizacije();
            },
            function(reason){
                console.log(reason);
            });
        }

        this.dobaviLekare = function() {
            // Upucuje se get zahtev na relativni URL api/proizvodi.
            $http.get("api/lekari").then(function(result){
                console.log(result);
                that.lekari = result.data;
            },
            function(reason) {
                console.log(reason);
            });
        }

        // Funkcija za uklanjanje proizvoda.
        this.ukloniLekara = function(id) {
            // Pri uklanjanju proizvoda serveru se salje delete zahtev
            // na url api/proizvodi/<id> pri cemu id zavisi od proizvoda
            // koji je neophodno obrisati.
            $http.delete("api/lekar/" + id).then(function(response){
                console.log(response);
                that.dobaviLekare();
            },
            function(reason){
                console.log(reason);
            });
        }
        
        this.dobaviSpecijalizacije();
        this.dobaviLekare();
    }]);
})(angular);