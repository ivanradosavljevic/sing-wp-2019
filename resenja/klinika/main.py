import flask
import datetime
from flask import Flask

from flaskext.mysql import MySQL # Importovanje klase za rad sa MySQL serverom.
from flaskext.mysql import pymysql # Importovanje paketa za podesavanje tipa kursora.

# Kreira se instanca flask aplikacije.
# Argument static_url_path odredjuje prefiks URL-a na koji ce se mapirati
# sadrzaj direktorijuma static. Podrazumevano je static. U ovom slucaju
# podeseno je da se mapira na korenski URL. To znaci da se zarad pristupanja
# statickim datotekama na serveru ne mora kucati URL localhost:5000/static/ime_datoteke
# vec da je dovoljno uneti localhost:5000/ime_datoteke. Sav staticki sadrzaj
# i dalje mora biti u direktorijumu static.
app = Flask(__name__, static_url_path="")

#Podesavanje konfiguracije za povezivanje na bazu podataka.
app.config["MYSQL_DATABASE_USER"] = "root" # Korisnicko ime.
app.config["MYSQL_DATABASE_PASSWORD"] = "levo.desno" # Lozinka.
app.config["MYSQL_DATABASE_DB"] = "klinika" # Naziv seme baze podataka na koju se treba povezati

# Instanciranje objekta za upravljanje konekcijama sa bazom podataka.
mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor) # Potrebno je proslediti flask aplikaciju iz koje
                                                           # je moguce procitati konfiguraciju, u ovom slucaju
                                                           # to je app instanca flask aplikacije.
                                                           # Argument cursorclass se koristi za podesavanje tipa
                                                           # kursora. U ovom slucaju kurosr je podesen da bude
                                                           # DictCursor sto znaci da ce rezultati upita biti vracani
                                                           # kao liste recnika ciju kljucevi su nazivi kolona a vrednosti
                                                           # vrednosti u datim kolonama. Ukliko se izostavi podesavanje
                                                           # za tip kursora koristi se podrazumevani tip kursora koji
                                                           # vraca torke torki u kojima se, redom navedenim u upitu
                                                           # nalaze vrednosti koje su rezultat upita.

# Definisan je obradjivac zahteva upucenih na korenski URL.
# Moguce je navesti vise ruta za jedan obradjivac zahteva.
@app.route("/")
@app.route("/index")
# Sve rute se mapiraju na funkciju odmah ispod njih.
# Vezivanje ruta za funkciju realizovano je upotrebom dekoratora.
def index_page():
    # Kada korisnik pristupi bilo kojoj od navedenih
    # ruta bice mu prikazana index.html stranica.
    return app.send_static_file("index.html")

#---------------------  SPECIJALIZACIJE ---------------------------------------
@app.route("/api/specijalizacije", methods=["GET"])
def dobavi_specijalizacije():
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM specijalizacija WHERE aktivan>0") 
    specijalizacije = cursor.fetchall() # Dobavljanje svih rezultat prethodni izvrsenog upita.
    return flask.jsonify(specijalizacije) # Vracanje proizvoda u JSON formatu.
                                    # Funkcija jsonify pretvara prosledjeni objekat u JSON format
                                    # i pakuje ga u response objekat u kojem su podesena sva
                                    # neophodna zaglavlja. Upotreba dumps funkcije iz
                                    # JSON modula vratila bi JSON ali ne bi napravila i response
                                    # objekat, koji bi se morao napraviti naknadno rucno.

@app.route("/api/specijalizacija/<int:id_specijalizacije>")
def dobavi_specijalizaciju(id_specijalizacije, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM specijalizacija WHERE id=%s", (id_specijalizacije,))
    specijalizacija = cursor.fetchone() # Kako je rezultat upita samo jedna torka dovoljno je
                                 # pozvati metodu fetchone() nad kursorom koja vraca
                                 # sledecu torku koja zadovoljava upit.
    if specijalizacija is not None:
        return flask.jsonify(specijalizacija) # Ukoliko je pronadje, proizvod se prosledjuje klijentu.
    else:
        return "", 404 # Ukoliko proizvod nije pronadjen klijent ce dobiti status odgovora 404.
                       # Odnosno podatak da trazeni resurs nije pronadjen.

@app.route("/api/specijalizacija", methods=["POST"])
def dodaj_specijalizaciju():
    db = mysql.get_db() # Dobavljanje instance konekcije ka bazi.
    cursor = db.cursor() # Dobavljanje kursora.

    cursor.execute("INSERT INTO specijalizacija(naziv, aktivan) VALUES(%(naziv)s,  %(aktivan)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201 # Status kod 201 oznacava uspesno kreiran resurs.

@app.route("/api/specijalizacija/<int:id_specijalizacije>", methods=["PUT"])
def izmeni_specijalizaciju(id_specijalizacije):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_specijalizacije # Id proizvoda koji treba azurirati preuzima
                              # se iz vrednosti parametra URL-a.
    cursor.execute("UPDATE specijalizacija SET naziv=%(naziv)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

# Uklanjanje proizvoda vrsi se po id-ju proizvoda.
# Id ce biti prosledjen kao parametar URL-a.
@app.route("/api/specijalizacija/<int:id_specijalizacije>", methods=["DELETE"])
def ukloni_specijalizaciju(id_specijalizacije):
    db = mysql.get_db()
    cursor = db.cursor()
    # cursor.execute("DELETE FROM proizvod WHERE id=%s", (id_proizvoda,))
    cursor.execute("UPDATE specijalizacija SET aktivan = 0 WHERE id=%s", (id_specijalizacije,))
    db.commit()
    return "", 204 # Operacija je uspesna ali je telo odgovora prazno.

#------------------------------------------------------------


#---------------------  LEKARI ---------------------------------------
@app.route("/api/lekari", methods=["GET"])
def dobavi_lekare():
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM lekar WHERE aktivan>0") 
    lekari = cursor.fetchall() # Dobavljanje svih rezultat prethodni izvrsenog upita.
    return flask.jsonify(lekari) # Vracanje proizvoda u JSON formatu.
                                    # Funkcija jsonify pretvara prosledjeni objekat u JSON format
                                    # i pakuje ga u response objekat u kojem su podesena sva
                                    # neophodna zaglavlja. Upotreba dumps funkcije iz
                                    # JSON modula vratila bi JSON ali ne bi napravila i response
                                    # objekat, koji bi se morao napraviti naknadno rucno.

@app.route("/api/lekar/<int:id_lekara>")
def dobavi_lekara(id_lekara, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM lekar WHERE id=%s", (id_lekara,))
    lekar = cursor.fetchone() # Kako je rezultat upita samo jedna torka dovoljno je
                                 # pozvati metodu fetchone() nad kursorom koja vraca
                                 # sledecu torku koja zadovoljava upit.
    if lekar is not None:
        return flask.jsonify(lekar) # Ukoliko je pronadje, proizvod se prosledjuje klijentu.
    else:
        return "", 404 # Ukoliko proizvod nije pronadjen klijent ce dobiti status odgovora 404.
                       # Odnosno podatak da trazeni resurs nije pronadjen.

@app.route("/api/lekar", methods=["POST"])
def dodaj_lekara():
    db = mysql.get_db() # Dobavljanje instance konekcije ka bazi.
    cursor = db.cursor() # Dobavljanje kursora.

    cursor.execute("INSERT INTO lekar(specijalizacija_id, ime, prezime, aktivan) VALUES(%(specijalizacija_id)s,  %(ime)s,  %(prezime)s ,  %(aktivan)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201 # Status kod 201 oznacava uspesno kreiran resurs.

@app.route("/api/lekar/<int:id_lekara>", methods=["PUT"])
def izmeni_lekara(id_lekara):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_lekara # Id proizvoda koji treba azurirati preuzima
                              # se iz vrednosti parametra URL-a.
    cursor.execute("UPDATE lekar SET specijalizacija_id=%(specijalizacija_id)s, ime=%(ime)s, prezime=%(prezime)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@app.route("/api/lekar/<int:id_lekara>", methods=["DELETE"])
def ukloni_lekara(id_lekara):
    db = mysql.get_db()
    cursor = db.cursor()
    # cursor.execute("DELETE FROM proizvod WHERE id=%s", (id_proizvoda,))
    cursor.execute("UPDATE lekar SET aktivan = 0 WHERE id=%s", (id_lekara,))
    db.commit()
    return "", 204 # Operacija je uspesna ali je telo odgovora prazno.

#------------------------------------------------------------



if __name__ == "__main__":
    # Aplikacija se pokrece tako da joj je moguce pristupa
    # od "spolja", odnosno sa udaljenih racunara. Ovo je postignuto
    # postavljanjem vrednosti prvog argumenta na ip adresu 0.0.0.0.
    # Potom je, narednim argumentom, navedeno da aplikacija slusa na port 5000.
    # Postavljanje vrednosti imenovanog argumenta threaded na True dovodi do toga
    # da ce se svaki zahtev obradjivati u odvojenom thread-u.
    # To znaci da prilikom obrade jednog zahteva nece doci do blokiranja
    # izvrsavanja cele aplikacije ili obrade drugih zahteva.
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.