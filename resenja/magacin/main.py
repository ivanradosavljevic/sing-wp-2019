import flask
from flask import Flask

# Kreira se instanca flask aplikacije.
# Argument static_url_path odredjuje prefiks URL-a na koji ce se mapirati
# sadrzaj direktorijuma static. Podrazumevano je static. U ovom slucaju
# podeseno je da se mapira na korenski URL. To znaci da se zarad pristupanja
# statickim datotekama na serveru ne mora kucati URL localhost:5000/static/ime_datoteke
# vec da je dovoljno uneti localhost:5000/ime_datoteke. Sav staticki sadrzaj
# i dalje mora biti u direktorijumu static.
app = Flask(__name__, static_url_path="")

dobavljaci = [] # Globalna promenljiva koja predstavlja kolekciju dobavljaca. !Veoma losa praksa. Upotrebljivo samo kao primer!
stavke = []
stavke_na_stanju = []

@app.route("/dobavljaci")
def prikaz_tabele_dobavljaca():
    return flask.render_template("dobavljaci.tpl.html", dobavljaci=dobavljaci)

@app.route("/novi_dobavljac", methods=['GET'])
def novi_dobavljac():
    novi_dobavljac = {
        "naziv":"",
        "drzava":"Srbija",
        "grad": "Novi Sad",
        "ulica_i_broj":"",
        "aktivan": True
    }
    return flask.render_template("dobavljac.tpl.html", dobavljac=novi_dobavljac)

@app.route("/dobavljac/<int:indeks>", methods=['GET'])
def dobavljac(indeks):
    print('DOBAVLJAC ', indeks)
    dobavljac = dobavljaci[indeks]
    return flask.render_template("dobavljac.tpl.html", dobavljac=dobavljac)

@app.route("/obrisi_dobavljaca/<int:indeks>", methods=['GET'])
def obrisi_obavljaca(indeks):
    dobavljaci[indeks]['aktivan'] = False
    return prikaz_tabele_dobavljaca()

# Definisan je obradjivac zahteva upucenih na korenski URL.
# Moguce je navesti vise ruta za jedan obradjivac zahteva.
@app.route("/")
@app.route("/index")
@app.route("/index.html")
# Sve rute se mapiraju na funkciju odmah ispod njih.
# Vezivanje ruta za funkciju realizovano je upotrebom dekoratora.
def index_page():
    # Obradjivac zahteva kao odgovor na zahtev sada salje sadrzaj datoteka
    # forma_post.html. Funkcija send_file prima putanju do datoteke ciji sadrzaj je potrebno
    # poslati kao odgovor. Putanja je relativna u odnosu na putanju sa koje se
    # izvrsava skripta.
    #return flask.send_file("index.html")
    return prikaz_tabele_dobavljaca()


# Ukoliko je zahtev upucen na URL /dodavanje_dobavljaca i pri tome
# je tip zahteva POST funkcija dodavanje_dobavljaca ce vrsiti obradu
# zahteva.
@app.route("/dodavanje_dobavljaca", methods=["POST"])
def dodavanje_dobavljaca():
    dobavljaci.append({
            "naziv": flask.request.form["naziv"],
            "drzava": flask.request.form["drzava"],
            "grad": flask.request.form["grad"],
            "ulica_i_broj": flask.request.form["ulica_i_broj"],
            "aktivan": True
        })
    return prikaz_tabele_dobavljaca()

# Kako se sada aplikacija pokrece u odvojenom
# kontejneru a ne direktno kao python skritpa
# neophodno je dodati proveru da li je modul
# pokrenut direktno preko interpretera. Ukoliko
# je modul pokrenut direktno vrednost promenljive
# __name__ ce biti string __main__ pa ce se
# aplikacija pokrenuti pozivom metode run nad
# instancom klase Flask. Ukoliko je modul
# pokrenut iz kontejnera ovaj uslov nece biti
# zadovoljen pa se ni poziv metode run nece desiti
# nego ce kontejner izvrsiti pokretanje Flask aplikacije
# onako kako je to u izabranom kontejneru predvidjeno.
if __name__ == "__main__":
    # Aplikacija se pokrece tako da joj je moguce pristupa
    # od "spolja", odnosno sa udaljenih racunara. Ovo je postignuto
    # postavljanjem vrednosti prvog argumenta na ip adresu 0.0.0.0.
    # Potom je, narednim argumentom, navedeno da aplikacija slusa na port 5000.
    # Postavljanje vrednosti imenovanog argumenta threaded na True dovodi do toga
    # da ce se svaki zahtev obradjivati u odvojenom thread-u.
    # To znaci da prilikom obrade jednog zahteva nece doci do blokiranja
    # izvrsavanja cele aplikacije ili obrade drugih zahteva.
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.